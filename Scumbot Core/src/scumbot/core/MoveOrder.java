/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package scumbot.core;

import java.util.LinkedList;
import scumbot.map.LakeMap;
import scumbot.map.MapTile;
import scumbot.map.Pathfinder;
import scumbot.system.Location;

/**
 *
 * @author MillerV
 */
public class MoveOrder implements Directive {
    private LinkedList<MapTile> path;
    private Location dest;
    private int priority;
    
    public MoveOrder(LakeMap map, Location cur, Location dest, int priority) {
        path = Pathfinder.findPath(map, cur, dest);
        this.priority = priority;
    }

    @Override
    public void update(ScumbotCore core) {
        core.getDirection();

        Location curLoc = new Location((int) (core.getY() / core.mapToWorld), (int) (core.getX() / core.mapToWorld));

        if (curLoc.equals(path.getFirst().getLocation())) {
            path.removeFirst();
        }

        if (path.isEmpty()) {
            core.leftMotor.set(0.0f);
            core.rightMotor.set(0.0f);
        }

        Location nextDest = path.getFirst().getLocation();

        float angle = Location.getDirection(curLoc, nextDest);

        float aDif = angle - core.heading;
        aDif = (aDif + 180) % 360 - 180;

        if (Math.abs(aDif) < 10) {
            float distance = Location.getDistance(curLoc, nextDest);
            if (distance > 5) {
                core.leftMotor.set(1.0f);
                core.rightMotor.set(1.0f);
            } else {
                core.leftMotor.set(distance / 5);
                core.rightMotor.set(distance / 5);
            }
        } else if (aDif < -30) {
            core.leftMotor.set(0);
            core.rightMotor.set(1.0f);
        } else if (aDif < 0) {
            core.leftMotor.set(0);
            core.rightMotor.set(aDif / -30);
        } else if (aDif > 30) {
            core.leftMotor.set(1.0f);
            core.rightMotor.set(0);
        } else {
            core.leftMotor.set(aDif / 30);
            core.rightMotor.set(0);
        }
    }

    @Override
    public boolean isDone() {
        return path.isEmpty();
    }

    @Override
    public int compareTo(Directive t) {
        return t.getPriority() - priority;
    }

    @Override
    public int getPriority() {
        return priority;
    }
}
