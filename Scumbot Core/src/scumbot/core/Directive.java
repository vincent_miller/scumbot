/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package scumbot.core;

/**
 *
 * @author MillerV
 */
public interface Directive extends Comparable<Directive> {
    public void update(ScumbotCore core);
    
    public boolean isDone();
    
    public int getPriority();
}
