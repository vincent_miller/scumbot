/*
 * Copyright 2013 Vincent Miller
 * 
 * This file is part of the CCRE, the Common Chicken Runtime Engine.
 * 
 * The CCRE is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * The CCRE is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CCRE.  If not, see <http://www.gnu.org/licenses/>.
 */
package scumbot.core;

import ccre.channel.BooleanStatus;
import ccre.channel.EventOutput;
import ccre.channel.EventStatus;
import ccre.channel.FloatOutput;
import ccre.channel.FloatStatus;
import ccre.ctrl.FloatMixing;
import ccre.ctrl.Mixing;
import ccre.log.LogLevel;
import ccre.log.Logger;
import ccre.obsidian.ObsidianCore;
import ccre.obsidian.ObsidianHardwareException;
import ccre.obsidian.PWMPin;
import ccre.obsidian.comms.ARPHandler;
import ccre.obsidian.comms.CommsID;
import ccre.obsidian.comms.ObsidianCommsNode;
import ccre.obsidian.comms.RobotConnection;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Random;
import scumbot.map.LakeMap;
import scumbot.system.Location;

public class ScumbotCore extends ObsidianCore {

    protected FloatOutput leftMotor = new FloatStatus();
    protected FloatOutput rightMotor = new FloatStatus();

    private EventOutput leftMotorRamping = new EventStatus();
    private EventOutput rightMotorRamping = new EventStatus();

    private FloatOutput conveyor = new FloatStatus();

    private LakeMap map;

    protected final float mapToWorld = 100;

    protected float heading; // in degrees
    protected float angularVelocity; // in degrees/s
    private long lastUpdate;

    private PriorityQueue<Directive> orders;

    private DataOutputStream sendPoints;

    private boolean mapLoaded = false;

    @Override
    protected void createRobotControl() {

        RobotConnection.startConnection("/dev/ttyUSB0", 9600, this);

        ObsidianCommsNode.globalNode.setARPHandler(new ARPHandler() {
            @Override
            public void onResponse(int[] ints) {
                Logger.info("ARP response from " + Arrays.toString(ints)); /*
                 ObsidianCommsNode.globalNode.sendDataRequest(CommsID.ID_DATA_MAP, ints);

                 new Thread() {
                 @Override
                 public void run() {
                 LakeMap tempMap = LakeMap.read(new DataInputStream(ObsidianCommsNode.globalNode.createInputStream(CommsID.ID_DATA_MAP)));
                 if (!mapLoaded) {
                 map = tempMap;
                 mapLoaded = true;
                 try {
                 DataOutputStream out = new DataOutputStream(new FileOutputStream(new File("map.bin")));
                 map.write(out);
                 } catch (FileNotFoundException e) {
                 throw new RuntimeException(e);
                 }
                 }
                 }
                 }.start();

                 new Timer().schedule(new TimerTask() {
                 @Override
                 public void run() {
                 try {
                 DataInputStream in = new DataInputStream(new FileInputStream(new File("map.bin")));
                 if (!mapLoaded) {
                 LakeMap tempMap = LakeMap.read(in);
                 if (!mapLoaded) {
                 map = tempMap;
                 mapLoaded = true;
                 }
                 }
                 } catch (FileNotFoundException e) {
                 throw new RuntimeException(e);
                 }
                 }
                 }, 50 * 1000);
                 * */

            }

            @Override
            public boolean onRequest(int[] ints) {
                return false;
            }
        });

        ObsidianCommsNode.globalNode.sendARPRequest();

        final FloatStatus xAxis = new FloatStatus();
        final FloatStatus yAxis = new FloatStatus();
        final BooleanStatus button1 = new BooleanStatus();

        ObsidianCommsNode.globalNode.addListener(CommsID.ID_FLOAT_JOYSTICK_X, xAxis);
        ObsidianCommsNode.globalNode.addListener(CommsID.ID_FLOAT_JOYSTICK_Y, yAxis);
        //ObsidianCommsNode.globalNode.addListener(CommsID.ID_BOOL_JOYSTICK_B1, button1);

        final FloatOutput printOutput = new FloatOutput() {
            @Override
            public void set(float f) {
                Logger.info("recieved " + f);
                File file = new File("logg.txt");
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                    for (Map.Entry<Thread, StackTraceElement[]> ent : Thread.getAllStackTraces().entrySet()) {
                        writer.write("Thread: " + ent.getKey() + ":" + ent.getValue().length);
                        writer.newLine();
                        for (StackTraceElement e : ent.getValue()) {
                            writer.write("Elem: " + e);
                            writer.newLine();
                        }
                    }
                    writer.close();
                } catch (IOException ex) {
                    Logger.log(LogLevel.SEVERE, null, ex);
                }
            }
        };

        xAxis.send(printOutput);
        yAxis.send(printOutput);

        duringTeleop.send(new EventOutput() {
            @Override
            public void event() {
                //Logger.info("left: " + (yAxis.readValue() + xAxis.readValue()));
                //Logger.info("right: " + (yAxis.readValue() - xAxis.readValue()));
                leftMotor.set(yAxis.get() + xAxis.get());
                rightMotor.set(yAxis.get() - xAxis.get());
                //conveyor.set(button1.get() ? 1.0f : 0.0f);
            }
        });

        final ScumbotCore core = this;

        duringAuto.send(new EventOutput() {
            @Override
            public void event() {
                if (mapLoaded && !orders.isEmpty()) {
                    orders.peek().update(core);
                    if (orders.peek().isDone()) {
                        orders.remove();
                    }
                }
            }
        });

        enabled.send(new EventOutput() {
            @Override
            public void event() {

                //FloatOutput o1 = makePWMOutput(PWMPin.P8_19, 0, 0.333f, 0.666f, 333f, true);
                //FloatOutput o2 = makePWMOutput(PWMPin.P9_16, 0, 0.333f, 0.666f, 333f, true);
                //conveyor = Mixing.combine(o1, o2);
                try {
                    FloatStatus temp1 = new FloatStatus();
                    leftMotorRamping = FloatMixing.createRamper(0.1f, temp1, FloatMixing.negate(makePWMOutput(PWMPin.P9_14, 0, 0.333f, 0.666f, 333f, true)));
                    leftMotor = FloatMixing.negate((FloatOutput) temp1);
                    periodic.send(leftMotorRamping);
                } catch (ObsidianHardwareException ex) {
                    Logger.log(LogLevel.SEVERE, "Could not acquire PWM", ex);
                }

                try {
                    FloatStatus temp2 = new FloatStatus();
                    rightMotorRamping = FloatMixing.createRamper(0.1f, temp2, makePWMOutput(PWMPin.P8_13, 0, 0.333f, 0.666f, 333f, true));
                    rightMotor = temp2;
                    periodic.send(rightMotorRamping);
                } catch (ObsidianHardwareException ex) {
                    Logger.log(LogLevel.SEVERE, "Could not acquire PWM", ex);
                }
            }
        });

        disabled.send(new EventOutput() {
            @Override
            public void event() {
                periodic.unsend(leftMotorRamping);
                periodic.unsend(rightMotorRamping);
                leftMotor = new FloatStatus();
                rightMotor = new FloatStatus();
                conveyor = new FloatStatus();

                try {
                    destroyPWMOutput(PWMPin.P8_13);
                } catch (ObsidianHardwareException ex) {
                    Logger.log(LogLevel.SEVERE, "Could not destroy PWM: ", ex);
                }

                try {
                    destroyPWMOutput(PWMPin.P9_14);
                } catch (ObsidianHardwareException ex) {
                    Logger.log(LogLevel.SEVERE, "Could not destroy PWM: ", ex);
                }
                xAxis.set(0);
                yAxis.set(0);
            }
        });
    }

    protected void calibrateCompass() {
        Runtime r = Runtime.getRuntime();
        try {
            leftMotor.set(1.0f);
            rightMotor.set(-1.0f);
            Process p = r.exec("python hmc_calib.py");
            p.waitFor();
        } catch (IOException ex) {
            Logger.log(LogLevel.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.log(LogLevel.SEVERE, null, ex);
        } finally {
            leftMotor.set(0.0f);
            rightMotor.set(0.0f);
        }
    }

    protected float getDirection() {
        Runtime r = Runtime.getRuntime();
        try {
            Process p = r.exec("python hmc.py");
            p.waitFor();
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s = b.readLine();
            b.close();
            p.destroy();
            float f = Float.parseFloat(s);
            angularVelocity = (f - heading) / ((System.currentTimeMillis() - lastUpdate) / 1000.0f);
            lastUpdate = System.currentTimeMillis();
            heading = f;
            return f;
        } catch (IOException ex) {
            Logger.log(LogLevel.SEVERE, null, ex);
            return 0;
        } catch (InterruptedException ex) {
            Logger.log(LogLevel.SEVERE, null, ex);
            return 0;
        } catch (NumberFormatException ex) {
            Logger.log(LogLevel.SEVERE, null, ex);
            return 0;
        }
    }

    private float getGPSX() {
        try {
            return getGPSX();
        } catch (StackOverflowError e) {
            Logger.log(LogLevel.SEVERE, "Stack limit reached. Please increase Java's maximum stack size.");
            return new Random().nextFloat();
        }
    }

    private void moveOrder(float lat, float lon, int priority) {
        Location destLoc = new Location((int) (lon / mapToWorld), (int) (lat / mapToWorld));
        Location curLoc = new Location((int) (getY() / mapToWorld), (int) (getX() / mapToWorld));
        orders.add(new MoveOrder(map, curLoc, destLoc, priority));
    }

    protected float getX() {
        throw new RuntimeException("no.");
    }

    protected float getY() {
        throw new RuntimeException("no.");
    }
}
