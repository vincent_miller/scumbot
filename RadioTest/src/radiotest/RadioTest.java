/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package radiotest;

import ccre.log.Logger;
import ccre.obsidian.comms.ARPHandler;
import ccre.obsidian.comms.CommsID;
import ccre.obsidian.comms.ObsidianCommsNode;
import ccre.obsidian.comms.XBeeRadio;
import com.rapplogic.xbee.api.XBeeException;

/**
 *
 * @author MillerV
 */
public class RadioTest {

    // This is the one with the big antenna.
    public static final int[] addr1 = new int[]{0x00, 0x13, 0xA2, 0x00, 0x40, 0xA1, 0x8F, 0x1B};

    // The pathetic one with the wire antenna.
    public static final int[] addr2 = new int[]{0x00, 0x13, 0xA2, 0x00, 0x40, 0xA8, 0xC4, 0x10};

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        XBeeRadio radio = new XBeeRadio("COM6", 9600);
        
        try {
            radio.open();
        } catch (XBeeException ex) {
            
        }
        
        ObsidianCommsNode.createGlobalNode(false, radio);
        ObsidianCommsNode.globalNode.setARPHandler(new ARPHandler() {
            @Override
            public void onResponse(int[] ints) {
                Logger.info("response");
                ObsidianCommsNode.globalNode.sendDataRequest(CommsID.ID_DATA_MAP, ints);
            }

            @Override
            public boolean onRequest(int[] ints) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        ObsidianCommsNode.globalNode.sendARPRequest();
    }
}
