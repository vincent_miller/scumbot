package ccre.obsidian;

import ccre.log.LogLevel;
import ccre.log.Logger;

/**
 *
 * @author MillerV
 */
public class ScumbotWorld extends WorldModule {

    private float x = 0, y = 0, heading = 0, linearVelocity = 0, rotationalVelocity = 0;

    private final int gpsXChan = 1, gpsYChan = 2, compassChan = 3, leftMotorChan = 13, rightMotorChan = 14 + 46;

    @Override
    public void periodic(EmulatorGUI gui) {
        float leftMotor = gui.getPin(leftMotorChan).getFloat();
        float rightMotor = gui.getPin(rightMotorChan).getFloat();

        linearVelocity = rightMotor + leftMotor;
        rotationalVelocity = rightMotor - leftMotor;

        heading = (heading + rotationalVelocity) % 1.0f;

        x += Math.cos(heading / (2 * Math.PI)) * linearVelocity;
        y += Math.sin(heading / (2 * Math.PI)) * linearVelocity;

        //gui.getPin(gpsXChan).set(x);
        //gui.getPin(gpsYChan).set(y);
        gui.getPin(compassChan).set(heading);
        
        Logger.log(LogLevel.INFO, x + "");
    }
}
