package ccre.obsidian;

import ccre.cluck.CluckGlobals;
import ccre.log.NetworkAutologger;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 *
 * @author Vincent Miller
 */
public class ScumbotEmulator extends EmulatorLauncher {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
        if (args.length < 1) {
            System.err.println("Expected arguments: <Obsidian-Jar>");
            System.exit(-1);
            return;
        }
        CluckGlobals.ensureInitializedCore();
        NetworkAutologger.register();
        URLClassLoader classLoader = new URLClassLoader(new URL[]{new File(args[0]).toURI().toURL()}, EmulatorLauncher.class.getClassLoader());

        ScumbotEmulator l = new ScumbotEmulator(classLoader);
        l.addWorldModule(new ScumbotWorld());
        l.main();
    }
    
    public ScumbotEmulator(ClassLoader coreClass) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        super(coreClass, true);
    }
}
