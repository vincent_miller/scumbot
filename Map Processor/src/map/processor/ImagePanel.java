package map.processor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {

    private int width, height;
    private Graphics2D pen;
    private BufferedImage image;
    private Color backgroundColor;

    public ImagePanel(int w, int h) {
        this.width = w;
        this.height = h;

        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        pen = image.createGraphics();
        backgroundColor = Color.WHITE;

        clear();
        setPreferredSize(new Dimension(width, height));
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent event) {
                Dimension d = getSize();
                setPreferredSize(d);
                width = d.width;
                height = d.height;
                BufferedImage oldBuffer = image;
                image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                pen = image.createGraphics();
                updateBufferAfterResize(oldBuffer);
            }
        });
    }
    
    public Graphics2D getPen() {
        return pen;
    }
    
    public BufferedImage getImage() {
        return image;
    }
    
    public int getImageWidth() {
        return width;
    }
    
    public int getImageHeght() {
        return height;
    }
    
    private void updateBufferAfterResize(BufferedImage oldBuffer) {
        clear();
        if (oldBuffer != null) {
            pen.drawImage(oldBuffer, 0, 0, this);
        }
        repaint();
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        if (image != null) {
            g.drawImage(image, 0, 0, this);
        }
    }
    
    public void setBackgroundColor(Color color) {
        backgroundColor = color;
        clear();
    }

    public final void clear() {
        pen.setColor(backgroundColor);
        pen.fillRect(0, 0, width, height);
    }
}
