/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scumbot.system;

import ccre.log.LogLevel;
import ccre.log.Logger;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import scumbot.map.LakeMap;

/**
 *
 * @author millerv
 */
public class SystemLoader {

    public static ScumbotSystem loadSystem(Reader jsonSource) {
        JSONObject json = readJSON(jsonSource);

        String name = json.getString("Name");
        JSONArray jsonBots = json.getJSONArray("Bots");
        HashMap<String, Scumbot> bots = readBotArray(jsonBots);

        LakeMap map;
        try {
            map = LakeMap.read(new DataInputStream(new FileInputStream(json.getString("Map"))));
            return new ScumbotSystem(name, map, bots);
        } catch (Exception e) {
            Logger.log(LogLevel.SEVERE, "Error reading map: ", e);
            return new ScumbotSystem(name, new LakeMap(), bots);
        }
    }

    public static HashMap<String, Scumbot> readBotArray(JSONArray botArray) {
        HashMap<String, Scumbot> bots = new HashMap<String, Scumbot>(botArray.size());

        for (Object obj : botArray) {
            JSONObject jsonBot = (JSONObject) obj;
            String name = jsonBot.getString("Name");
            String rawAddr = jsonBot.getString("Address");
            String[] addr = rawAddr.split(", ");
            int[] address = new int[addr.length];
            for (int i = 0; i < addr.length; i++) {
                address[i] = Integer.parseInt(addr[i].toLowerCase(), 16);
            }
            bots.put(name, new Scumbot(name, address));
        }
        return bots;
    }

    public static JSONObject readJSON(Reader jsonSource) {
        BufferedReader reader = new BufferedReader(jsonSource);

        String jsonText = "";
        String s;

        try {
            while ((s = reader.readLine()) != null) {
                jsonText += s;
            }
        } catch (java.io.IOException e) {
            Logger.log(LogLevel.SEVERE, "Error: could not read the system config file.");
        }

        return (JSONObject) JSONSerializer.toJSON(jsonText);
    }
}
