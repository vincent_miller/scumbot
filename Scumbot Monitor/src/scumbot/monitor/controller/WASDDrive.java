/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package scumbot.monitor.controller;

import ccre.channel.FloatStatus;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *
 * @author MillerV
 */
public class WASDDrive extends KeyAdapter {
    public FloatStatus ws = new FloatStatus();
    public FloatStatus ad = new FloatStatus();
    
    private final float moveSpeed;
    private final float turnSpeed;
    
    public WASDDrive(float moveSpeed, float turnSpeed) {
        this.moveSpeed = moveSpeed;
        this.turnSpeed = turnSpeed;
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_W && ws.get() != moveSpeed) {
            ws.set(moveSpeed);
        } else if (e.getKeyCode() == KeyEvent.VK_S && ws.get() != -moveSpeed) {
            ws.set(-moveSpeed);
        } else if (e.getKeyCode() == KeyEvent.VK_D && ad.get() != turnSpeed) {
            ad.set(turnSpeed);
        } else if (e.getKeyCode() == KeyEvent.VK_A && ad.get() != -turnSpeed) {
            ad.set(-turnSpeed);
        }
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_W || e.getKeyCode() == KeyEvent.VK_S) {
            ws.set(0);
        } else if (e.getKeyCode() == KeyEvent.VK_D || e.getKeyCode() == KeyEvent.VK_A) {
            ad.set(0);
        }
    }
}
