/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scumbot.monitor;

import ccre.channel.BooleanStatus;
import ccre.channel.EventStatus;
import ccre.channel.FloatStatus;
import ccre.ctrl.FloatMixing;
import ccre.ctrl.Mixing;
import ccre.log.LogLevel;
import ccre.log.Logger;
import ccre.obsidian.comms.ARPHandler;
import ccre.obsidian.comms.CommsID;
import ccre.obsidian.comms.DataRequestHandler;
import ccre.obsidian.comms.ObsidianCommsNode;
import ccre.obsidian.comms.XBeeRadio;
import com.rapplogic.xbee.api.XBeeException;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import scumbot.map.LakeMap;
import scumbot.map.MapTile;
import scumbot.monitor.controller.JoystickMonitor;
import scumbot.monitor.controller.WASDDrive;
import scumbot.system.Location;
import scumbot.system.SystemLoader;
import scumbot.system.Scumbot;
import scumbot.system.ScumbotSystem;

/**
 *
 * @author MindexllerV
 */
public class GUI extends javax.swing.JFrame {

    private final JoystickMonitor joystickMonitor = new JoystickMonitor(1);

    private final JFileChooser fileChooser = new JFileChooser();

    private final ImagePanel mapViewPanel;

    //private final ImagePanel mapEditPanel;
    private final ScumbotSystem system;

    private EventStatus heartbeatTimer;
    private EventStatus heartbeat;

    private BooleanStatus enabled;

    private FloatStatus joystickX;

    private FloatStatus joystickY;

    private BooleanStatus joystickBtn1;

    private XBeeRadio radio;

    private boolean server = false;

    // This is the one with the big antenna.
    public static final int[] addr1 = new int[]{0x00, 0x13, 0xA2, 0x00, 0x40, 0xA1, 0x8F, 0x1B};

    // The pathetic one with the wire antenna.
    public static final int[] addr2 = new int[]{0x00, 0x13, 0xA2, 0x00, 0x40, 0xA8, 0xC4, 0x10};

    private final WASDDrive wasd;

    private int index = 0;

    private String radioPort = "COM1";

    private File saveFile;

    private DefaultListModel listModel;

    /**
     * Creates new form GUI
     */
    public GUI() {
        heartbeatTimer = new EventStatus();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (server) {
                    heartbeatTimer.event();
                }
            }
        }, 500, 500);

        enabled = new BooleanStatus();
        enabled.set(false);
        wasd = new WASDDrive(1.0f, 1.0f);
        BasicConfigurator.configure();
        org.apache.log4j.Logger.getRootLogger().setLevel(Level.INFO);
        initComponents();
        panelMainButtons.setFocusable(true);
        panelMainButtons.requestFocusInWindow();
        try {
            system = SystemLoader.loadSystem(new FileReader("../Scumbot System/sampleSystem.json"));
            for (Scumbot s : system.getBots()) {
                System.out.println(s);
            }
            //system.clearBots();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        mapViewPanel = new ImagePanel(1000, 1000);
        //imagePanel = new ImagePanel(system.getMap().getNumCols() * 32, system.getMap().getNumRows() * 32);
        mapViewPanel.setBackgroundColor(Color.GREEN);
        mapEditPanel.setBackgroundColor(Color.GREEN);
        mapEditPanel.addSystem(system);

        tabs.add(mapViewPanel, "Map View");
        //drawMap();
        //LinkedList<MapTile> path = Pathfinder.findPath(system.getMap(), new Location(3, 7), new Location(13, 7));
        //drawPath(path);

        Enumeration<?> ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            final CommPortIdentifier id = (CommPortIdentifier) ports.nextElement();
            radioPort = id.getName();
            JMenuItem item = new JMenuItem(id.getName());
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    radioPort = id.getName();
                    SerialPort i;
                }
            });
            menuEditPort.add(item);
        }

        setTitle("Scumbot Monitor");
        listModel = new DefaultListModel();
        logArea.setModel(listModel);
        Logger.addTarget(new ListModelLogger(listModel, logArea));
        scrollPaneLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());
            }
        });
    }

    private float getMinLat() {
        return Float.parseFloat(fieldMinLat.getText());
    }

    private float getMinLon() {
        return Float.parseFloat(fieldMinLon.getText());
    }

    private float getMaxLat() {
        return Float.parseFloat(fieldMaxLat.getText());
    }

    private float getMaxLon() {
        return Float.parseFloat(fieldMaxLon.getText());
    }

    private void drawPath(LinkedList<MapTile> path) {
        Graphics pen = mapViewPanel.getPen();
        pen.setColor(Color.RED);
        for (MapTile tile : path) {
            Location loc = tile.getLocation();
            pen.fillOval(loc.col * 32 + 8, loc.row * 32 + 8, 16, 16);
        }
    }

    private void drawMap() {
        Graphics pen = mapViewPanel.getPen();
        for (int col = 0; col < system.map.getNumCols(); col++) {
            for (int row = 0; row < system.map.getNumRows(); row++) {
                if (system.map.getTileType(new Location(row, col)) == MapTile.Type.WATER) {
                    pen.setColor(Color.BLUE);
                } else {
                    pen.setColor(Color.GREEN);
                }
                pen.fillRect(col * 32, row * 32, 32, 32);
            }
        }
        mapViewPanel.repaint();
    }

    private void connect() {
        final GUI gui = this;
        new Thread() {
            @Override
            public void run() {
                btnConnect.setEnabled(false);
                try {
                    if (!server) {
                        radio = new XBeeRadio(radioPort, 9600);
                        radio.open();

                        ObsidianCommsNode.createGlobalNode(false, radio);
                        ObsidianCommsNode.globalNode.setARPHandler(new ARPHandler() {
                            @Override
                            public void onResponse(int[] ints) {
                            }

                            @Override
                            public boolean onRequest(int[] ints) {
                                Logger.info("Robot connected: " + Arrays.toString(ints));
                                
                                for (Scumbot bot : system.getBots()) {
                                    if (Arrays.equals(bot.getAddress(), ints)) {
                                        return true;
                                    }
                                }

                                system.addBot(new Scumbot("anonymous" + index, ints));
                                enabled.send(ObsidianCommsNode.globalNode.createBooleanOutput(CommsID.ID_BOOL_ENABLED, ints));
                                index++;
                                return true;
                            }
                        });

                        ObsidianCommsNode.globalNode.addListener(CommsID.ID_DATA_MAP, new DataRequestHandler() {
                            @Override
                            public void onRequest(OutputStream out) {
                                Logger.info("got map request");
                                system.map.write(new DataOutputStream(out));
                                try {
                                    out.flush();
                                    out.close();
                                } catch (IOException e) {
                                    Logger.log(LogLevel.WARNING, "Error sending map: ", e);
                                }
                            }
                        });

                        heartbeat = new EventStatus();
                        heartbeatTimer.send(heartbeat);
                        enabled = new BooleanStatus();
                        enabled.set(false);
                        heartbeat.send(ObsidianCommsNode.globalNode.createEventConsumer(CommsID.ID_EVENT_HEARTBEAT, ObsidianCommsNode.BROADCAST));

                        for (Scumbot bot : system.getBots()) {
                            enabled.send(ObsidianCommsNode.globalNode.createBooleanOutput(CommsID.ID_BOOL_ENABLED, bot.getAddress()));
                        }
                        btnEnable.setEnabled(true);
                        btnRefresh.setEnabled(true);
                        Logger.info("Connected");
                        server = true;
                        btnConnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/power_off.png")));
                        btnRefresh.setEnabled(true);
                        btnConnect.setToolTipText("Stop Server");
                        refreshJoystick();
                    } else {
                        enabled.set(false);
                        heartbeatTimer.unsend(heartbeat);
                        if (joystickMonitor.isConnected()) {
                            joystickMonitor.axes[0].unsend(joystickX);
                            joystickMonitor.axes[1].unsend(joystickY);
                        } else {
                            wasd.ad.unsend(joystickX);
                            wasd.ws.unsend(joystickY);
                        }
                        btnEnable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/enable.png")));
                        btnEnable.setEnabled(false);
                        btnRefresh.setEnabled(false);
                        radio.close();
                        radio = null;
                        Logger.info("Disconnected");
                        server = false;
                        btnConnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/power_on.png")));
                        btnRefresh.setEnabled(false);
                        btnConnect.setToolTipText("Start Server");
                    }
                } catch (XBeeException e) {
                    Logger.log(LogLevel.WARNING, "Error while connecting: " + radioPort, e);
                }
                btnConnect.setEnabled(true);
            }
        }.start();
    }

    private void refreshJoystick() {
        new Thread() {
            @Override
            public void run() {

                boolean success = joystickMonitor.refresh(1);

                joystickX = new FloatStatus();
                joystickY = new FloatStatus();

                joystickBtn1 = new BooleanStatus();

                joystickX.send(ObsidianCommsNode.globalNode.createFloatOutput(CommsID.ID_FLOAT_JOYSTICK_X, ObsidianCommsNode.BROADCAST));
                joystickY.send(ObsidianCommsNode.globalNode.createFloatOutput(CommsID.ID_FLOAT_JOYSTICK_Y, ObsidianCommsNode.BROADCAST));
                //joystickBtn1.addTarget(ObsidianCommsNode.globalNode.createBooleanOutput(CommsID.ID_BOOL_JOYSTICK_B1, ObsidianCommsNode.BROADCAST));
                
                if (success) {
                    Logger.log(LogLevel.INFO, "Connected to joystick.");
                    joystickMonitor.axes[0].send(joystickX);
                    FloatMixing.negate(joystickMonitor.axes[1]).send(joystickY);
                    joystickMonitor.buttons[0].send(joystickBtn1);
                } else {
                    Logger.log(LogLevel.INFO, "Could not connect to joystick; use the WASD keys to drive.");
                    wasd.ad.send(joystickX);
                    wasd.ws.send(joystickY);
                }
                panelMainButtons.requestFocusInWindow();
            }
        }.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        splitPaneMain = new javax.swing.JSplitPane();
        splitPaneLog = new javax.swing.JSplitPane();
        panelMainButtons = new javax.swing.JPanel();
        btnRefresh = new javax.swing.JButton();
        btnEnable = new javax.swing.JButton();
        btnConnect = new javax.swing.JButton();
        btnClearLog = new javax.swing.JButton();
        scrollPaneLog = new javax.swing.JScrollPane();
        logArea = new javax.swing.JList();
        tabs = new javax.swing.JTabbedPane();
        splitPaneMapEdit = new javax.swing.JSplitPane();
        panelEditTools = new javax.swing.JPanel();
        fieldMinLat = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        fieldMinLon = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        fieldMaxLat = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        fieldMaxLon = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnLand = new javax.swing.JButton();
        btnWater = new javax.swing.JButton();
        btnEditVertex = new javax.swing.JButton();
        btnGPSPoint = new javax.swing.JButton();
        mapEditPanel = new scumbot.monitor.MapEditPanel();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        optionFileOpen = new javax.swing.JMenuItem();
        optionFileNew = new javax.swing.JMenuItem();
        optionFileSave = new javax.swing.JMenuItem();
        optionFileSaveAs = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        menuEditPort = new javax.swing.JMenu();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel3.setText("jLabel3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Scumbot Monitor");

        splitPaneMain.setDividerLocation(240);
        splitPaneMain.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        splitPaneLog.setDividerLocation(72);

        panelMainButtons.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                panelMainButtonsKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                panelMainButtonsKeyReleased(evt);
            }
        });

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/refresh.png"))); // NOI18N
        btnRefresh.setToolTipText("Refresh Joystick");
        btnRefresh.setEnabled(false);
        btnRefresh.setRequestFocusEnabled(false);
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnEnable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/enable.png"))); // NOI18N
        btnEnable.setToolTipText("Enable");
        btnEnable.setEnabled(false);
        btnEnable.setMaximumSize(new java.awt.Dimension(32, 32));
        btnEnable.setMinimumSize(new java.awt.Dimension(32, 32));
        btnEnable.setRequestFocusEnabled(false);
        btnEnable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnableActionPerformed(evt);
            }
        });

        btnConnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/power_on.png"))); // NOI18N
        btnConnect.setToolTipText("Start Server");
        btnConnect.setRequestFocusEnabled(false);
        btnConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConnectActionPerformed(evt);
            }
        });

        btnClearLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/clear.png"))); // NOI18N
        btnClearLog.setToolTipText("Clear Log");
        btnClearLog.setMaximumSize(new java.awt.Dimension(32, 32));
        btnClearLog.setMinimumSize(new java.awt.Dimension(32, 32));
        btnClearLog.setRequestFocusEnabled(false);
        btnClearLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearLogActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMainButtonsLayout = new javax.swing.GroupLayout(panelMainButtons);
        panelMainButtons.setLayout(panelMainButtonsLayout);
        panelMainButtonsLayout.setHorizontalGroup(
            panelMainButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMainButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEnable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConnect)
                    .addComponent(btnRefresh)
                    .addComponent(btnClearLog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelMainButtonsLayout.setVerticalGroup(
            panelMainButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnConnect, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEnable, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClearLog, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 9, Short.MAX_VALUE))
        );

        splitPaneLog.setLeftComponent(panelMainButtons);

        scrollPaneLog.setAutoscrolls(true);
        scrollPaneLog.setViewportView(logArea);

        splitPaneLog.setRightComponent(scrollPaneLog);

        splitPaneMain.setRightComponent(splitPaneLog);

        splitPaneMapEdit.setDividerLocation(160);

        fieldMinLat.setText("0.0");
        fieldMinLat.setToolTipText("");
        fieldMinLat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldMinLatActionPerformed(evt);
            }
        });

        jLabel1.setText("min lat");

        fieldMinLon.setText("0.0");
        fieldMinLon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldMinLonActionPerformed(evt);
            }
        });

        jLabel2.setText("min lon");

        fieldMaxLat.setText("0.0");
        fieldMaxLat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldMaxLatActionPerformed(evt);
            }
        });

        jLabel4.setText("max lat");

        fieldMaxLon.setText("0.0");
        fieldMaxLon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldMaxLonActionPerformed(evt);
            }
        });

        jLabel5.setText("max lon");

        btnLand.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/draw_land.png"))); // NOI18N
        btnLand.setToolTipText("Draw Land");
        btnLand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLandActionPerformed(evt);
            }
        });

        btnWater.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/draw_water.png"))); // NOI18N
        btnWater.setToolTipText("Draw Water");
        btnWater.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnWaterActionPerformed(evt);
            }
        });

        btnEditVertex.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/edit_vertex.png"))); // NOI18N
        btnEditVertex.setToolTipText("Edit Vertices");
        btnEditVertex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditVertexActionPerformed(evt);
            }
        });

        btnGPSPoint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/point_gps.png"))); // NOI18N
        btnGPSPoint.setToolTipText("Place GPS Point");
        btnGPSPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGPSPointActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelEditToolsLayout = new javax.swing.GroupLayout(panelEditTools);
        panelEditTools.setLayout(panelEditToolsLayout);
        panelEditToolsLayout.setHorizontalGroup(
            panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEditToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEditToolsLayout.createSequentialGroup()
                        .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2))
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fieldMaxLon)
                            .addComponent(fieldMaxLat)
                            .addComponent(fieldMinLat)
                            .addComponent(fieldMinLon)))
                    .addGroup(panelEditToolsLayout.createSequentialGroup()
                        .addComponent(btnLand, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnWater, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditVertex, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGPSPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelEditToolsLayout.setVerticalGroup(
            panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEditToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldMinLat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldMinLon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldMaxLat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldMaxLon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEditToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnLand, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(btnEditVertex, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(btnGPSPoint, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(btnWater, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(62, Short.MAX_VALUE))
        );

        splitPaneMapEdit.setLeftComponent(panelEditTools);

        javax.swing.GroupLayout mapEditPanelLayout = new javax.swing.GroupLayout(mapEditPanel);
        mapEditPanel.setLayout(mapEditPanelLayout);
        mapEditPanelLayout.setHorizontalGroup(
            mapEditPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        mapEditPanelLayout.setVerticalGroup(
            mapEditPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        splitPaneMapEdit.setRightComponent(mapEditPanel);

        tabs.addTab("Edit Map", splitPaneMapEdit);

        splitPaneMain.setLeftComponent(tabs);

        menuFile.setText("File");

        optionFileOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        optionFileOpen.setText("Open map...");
        optionFileOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionFileOpenActionPerformed(evt);
            }
        });
        menuFile.add(optionFileOpen);

        optionFileNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        optionFileNew.setText("New map");
        optionFileNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionFileNewActionPerformed(evt);
            }
        });
        menuFile.add(optionFileNew);

        optionFileSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        optionFileSave.setText("Save map");
        optionFileSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionFileSaveActionPerformed(evt);
            }
        });
        menuFile.add(optionFileSave);

        optionFileSaveAs.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        optionFileSaveAs.setText("Save map as...");
        optionFileSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionFileSaveAsActionPerformed(evt);
            }
        });
        menuFile.add(optionFileSaveAs);

        menuBar.add(menuFile);

        menuEdit.setText("Edit");

        menuEditPort.setText("XBee USB port");
        menuEditPort.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                menuEditPortMenuSelected(evt);
            }
        });
        menuEdit.add(menuEditPort);

        menuBar.add(menuEdit);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPaneMain)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPaneMain)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshJoystick();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnEnableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnableActionPerformed
        if (enabled.get()) {
            enabled.set(false);
            btnEnable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/enable.png")));
            Logger.info("Disabled.");
            btnEnable.setToolTipText("Enable");
        } else {
            enabled.set(true);
            btnEnable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/scumbot/monitor/images/disable.png")));
            Logger.info("Enabled.");
            btnEnable.setToolTipText("Disable");
        }
    }//GEN-LAST:event_btnEnableActionPerformed

    private void panelMainButtonsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panelMainButtonsKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            btnEnableActionPerformed(null);
        } else {
            wasd.keyPressed(evt);
        }
    }//GEN-LAST:event_panelMainButtonsKeyPressed

    private void panelMainButtonsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panelMainButtonsKeyReleased
        wasd.keyReleased(evt);
    }//GEN-LAST:event_panelMainButtonsKeyReleased

    private void optionFileOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionFileOpenActionPerformed
        int i = fileChooser.showOpenDialog(this);

        if (i == JFileChooser.APPROVE_OPTION) {
            File f = fileChooser.getSelectedFile();
            try {
                system.map = LakeMap.read(new DataInputStream(new FileInputStream(f)));
                saveFile = f;
                mapEditPanel.updateDisplay();
                Logger.log(LogLevel.INFO, system.map.regions.size() + "");
            } catch (FileNotFoundException e) {
                Logger.log(LogLevel.INFO, "Error opening map: ", e);
            }
        }
    }//GEN-LAST:event_optionFileOpenActionPerformed

    private void optionFileNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionFileNewActionPerformed
        system.map.regions.clear();
        mapEditPanel.selectedRegion = null;
        mapEditPanel.selectedVertex = -1;
        mapEditPanel.setTool(MapEditPanel.Tool.EDIT_VERTEX);
        mapEditPanel.updateDisplay();
        saveFile = null;
    }//GEN-LAST:event_optionFileNewActionPerformed

    private void optionFileSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionFileSaveActionPerformed
        if (saveFile == null) {
            optionFileSaveAsActionPerformed(evt);
        } else {
            try {
                system.map.write(new DataOutputStream(new FileOutputStream(saveFile)));
            } catch (Exception e) {
                Logger.log(LogLevel.WARNING, "Error saving: ", e);
            }
        }
    }//GEN-LAST:event_optionFileSaveActionPerformed

    private void btnLandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLandActionPerformed
        mapEditPanel.requestFocusInWindow();
        mapEditPanel.setTool(MapEditPanel.Tool.DRAW_LAND);
        mapEditPanel.selectedRegion = new LakeMap.Region(false);
        system.map.regions.add(mapEditPanel.selectedRegion);
    }//GEN-LAST:event_btnLandActionPerformed

    private void btnWaterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnWaterActionPerformed
        mapEditPanel.requestFocusInWindow();
        mapEditPanel.setTool(MapEditPanel.Tool.DRAW_WATER);
        mapEditPanel.selectedRegion = new LakeMap.Region(true);
        system.map.regions.add(mapEditPanel.selectedRegion);
    }//GEN-LAST:event_btnWaterActionPerformed

    private void fieldMinLatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldMinLatActionPerformed
        updateMapSize();
    }//GEN-LAST:event_fieldMinLatActionPerformed

    private void fieldMinLonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldMinLonActionPerformed
        updateMapSize();
    }//GEN-LAST:event_fieldMinLonActionPerformed

    private void fieldMaxLatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldMaxLatActionPerformed
        updateMapSize();
    }//GEN-LAST:event_fieldMaxLatActionPerformed

    private void fieldMaxLonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldMaxLonActionPerformed
        updateMapSize();
    }//GEN-LAST:event_fieldMaxLonActionPerformed

    private void btnEditVertexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditVertexActionPerformed
        mapEditPanel.setTool(MapEditPanel.Tool.EDIT_VERTEX);
    }//GEN-LAST:event_btnEditVertexActionPerformed

    private void btnGPSPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGPSPointActionPerformed
        try {
            ObsidianCommsNode.globalNode.sendDataRequest(CommsID.ID_DATA_POINT, addr1);
            DataInputStream dis = new DataInputStream(ObsidianCommsNode.globalNode.createInputStream(CommsID.ID_DATA_POINT));
            double x = dis.readDouble();
            double y = dis.readDouble();
            if (mapEditPanel.selectedRegion != null) {
                mapEditPanel.selectedRegion.polygon.addPoint(x, y);
            }
            dis.close();
        } catch (IOException ex) {
            Logger.log(LogLevel.WARNING, "Error reading recieved point data: ", ex);
        }
    }//GEN-LAST:event_btnGPSPointActionPerformed

    private void btnConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConnectActionPerformed
        connect();
    }//GEN-LAST:event_btnConnectActionPerformed

    private void menuEditPortMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_menuEditPortMenuSelected

    }//GEN-LAST:event_menuEditPortMenuSelected

    private void optionFileSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionFileSaveAsActionPerformed
        int i = fileChooser.showSaveDialog(this);

        if (i == JFileChooser.APPROVE_OPTION) {
            File f = fileChooser.getSelectedFile();
            try {
                system.map.write(new DataOutputStream(new FileOutputStream(f)));
                saveFile = f;
            } catch (Exception e) {
                Logger.log(LogLevel.WARNING, "Error saving: ", e);
            }
        }
    }//GEN-LAST:event_optionFileSaveAsActionPerformed

    private void btnClearLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearLogActionPerformed
        listModel.clear();
    }//GEN-LAST:event_btnClearLogActionPerformed

    private void updateMapSize() {
        try {
            float minLat = getMinLat();
            float minLon = getMinLon();
            float maxLat = getMaxLat();
            float maxLon = getMaxLon();

            mapEditPanel.setMapBounds(maxLat, minLat, maxLon, minLon);

        } catch (NumberFormatException e) {
            Logger.log(LogLevel.WARNING, "Error parsing number entry: ", e);
            JOptionPane.showMessageDialog(this, "Please enter a number.");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    //javax.swing.UIManager.setLookAndFeel(info.getClassName());

                    break;
                }
            }
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearLog;
    private javax.swing.JButton btnConnect;
    private javax.swing.JButton btnEditVertex;
    private javax.swing.JButton btnEnable;
    private javax.swing.JButton btnGPSPoint;
    private javax.swing.JButton btnLand;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnWater;
    private javax.swing.JTextField fieldMaxLat;
    private javax.swing.JTextField fieldMaxLon;
    private javax.swing.JTextField fieldMinLat;
    private javax.swing.JTextField fieldMinLon;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JList logArea;
    private scumbot.monitor.MapEditPanel mapEditPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuEditPort;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenuItem optionFileNew;
    private javax.swing.JMenuItem optionFileOpen;
    private javax.swing.JMenuItem optionFileSave;
    private javax.swing.JMenuItem optionFileSaveAs;
    private javax.swing.JPanel panelEditTools;
    private javax.swing.JPanel panelMainButtons;
    private javax.swing.JScrollPane scrollPaneLog;
    private javax.swing.JSplitPane splitPaneLog;
    private javax.swing.JSplitPane splitPaneMain;
    private javax.swing.JSplitPane splitPaneMapEdit;
    private javax.swing.JTabbedPane tabs;
    // End of variables declaration//GEN-END:variables
}
