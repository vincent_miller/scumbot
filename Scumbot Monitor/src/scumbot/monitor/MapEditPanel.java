/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scumbot.monitor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import javax.swing.JOptionPane;
import javax.swing.event.MouseInputAdapter;
import scumbot.map.LakeMap;
import scumbot.map.LakeMap.Region;
import scumbot.map.Polygon2D;
import scumbot.system.ScumbotSystem;

/**
 *
 * @author MillerV
 */
public class MapEditPanel extends ImagePanel {

    private Tool currentTool = Tool.EDIT_VERTEX;
    private ScumbotSystem system;

    private float viewLat = 0;
    private float viewLon = 0;

    // pixels per degree
    private float viewScale = 1;

    public Region selectedRegion;
    public int selectedVertex;

    private Cursor plus;

    private Point mousePrev;

    private boolean dragWorld = false;

    /**
     * Creates new form MapEditPanel
     */
    public MapEditPanel() {
        super(800, 600);
        initComponents();
        plus = new Cursor(Cursor.CROSSHAIR_CURSOR);

        setFocusable(true);
        final MapEditPanel panel = this;
        MouseInputAdapter m = new MouseInputAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                mousePrev = evt.getPoint();
                if (evt.getButton() == MouseEvent.BUTTON1) {
                    switch (currentTool) {
                        case DRAW_WATER:
                            selectedRegion.polygon.addPoint(screenToWorldX(evt.getX()), screenToWorldY(evt.getY()));
                            break;
                        case DRAW_LAND:
                            selectedRegion.polygon.addPoint(screenToWorldX(evt.getX()), screenToWorldY(evt.getY()));
                            break;
                        case EDIT_VERTEX:
                            if (selectVertex(evt.getPoint())) {
                                return;
                            } else {
                                int index = getSelectedSegment(evt.getPoint());
                                if (index != -1) {
                                    selectedRegion.polygon.addPoint(screenToWorldX(evt.getX()), screenToWorldY(evt.getY()), index);
                                    selectedVertex = index;
                                    panel.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                                } else {
                                    selectRegion(evt.getPoint());
                                }
                            }
                            break;
                    }
                } else if (evt.getButton() == MouseEvent.BUTTON2) {
                    dragWorld = true;
                } else if (evt.getButton() == MouseEvent.BUTTON3) {
                    if (currentTool == Tool.EDIT_VERTEX) {
                        if (selectVertex(evt.getPoint())) {
                            selectedRegion.polygon.removePoint(selectedVertex);
                            if (selectedRegion.polygon.npoints < 2) {
                                system.map.regions.remove(selectedRegion);
                                selectedRegion = null;
                            }
                        } else if (selectRegion(evt.getPoint())) {
                            String[] options = {"Yes", "No"};
                            int n = JOptionPane.showOptionDialog(panel, "Are you sure you want to delete this entire polygon?",
                                    "Confirm deletion",
                                    JOptionPane.YES_NO_CANCEL_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null,
                                    options,
                                    options[1]);
                            if (n == 0) {
                                system.map.regions.remove(selectedRegion);
                                selectedRegion = null;
                            }
                        }
                    }
                }

                updateDisplay();
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                if (evt.getButton() == MouseEvent.BUTTON1) {
                    selectedVertex = -1;
                } else if (evt.getButton() == MouseEvent.BUTTON2) {
                    dragWorld = false;
                }
            }

            @Override
            public void mouseDragged(MouseEvent evt) {
                if (!dragWorld && currentTool == Tool.EDIT_VERTEX && selectedRegion != null) {
                    Polygon2D p = selectedRegion.polygon;
                    if (selectedVertex >= 0) {
                        p.xpoints[selectedVertex] -= (mousePrev.x - evt.getX()) / viewScale;
                        p.ypoints[selectedVertex] -= (mousePrev.y - evt.getY()) / viewScale;
                    } else {
                        for (int i = 0; i < p.npoints; i++) {
                            p.xpoints[i] -= (mousePrev.x - evt.getX()) / viewScale;
                            p.ypoints[i] -= (mousePrev.y - evt.getY()) / viewScale;
                        }
                    }
                    p.invalidate();
                } else if (dragWorld) {
                    viewLon += (mousePrev.x - evt.getX()) / viewScale;
                    viewLat += (mousePrev.y - evt.getY()) / viewScale;
                }
                mousePrev = evt.getPoint();
                updateDisplay();
            }

            @Override
            public void mouseMoved(MouseEvent evt) {
                updateCursor(evt);
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent evt) {
                double multiplier = Math.pow(1.1, -evt.getWheelRotation());

                double oldScale = viewScale;
                viewScale *= multiplier;

                viewLon += (getImageWidth() / oldScale - getImageWidth() / viewScale) / 2;
                viewLat += (getImageHeight() / oldScale - getImageHeight() / viewScale) / 2;

                updateDisplay();
                updateCursor(evt);
            }
        };

        addMouseListener(m);
        addMouseMotionListener(m);
        addMouseWheelListener(m);
    }

    private int worldToScreenX(double x) {
        return (int) ((x - viewLon) * viewScale);
    }

    private int worldToScreenY(double y) {
        return (int) ((y - viewLat) * viewScale);
    }

    private double screenToWorldX(int x) {
        return x / viewScale + viewLon;
    }

    private double screenToWorldY(int y) {
        return y / viewScale + viewLat;
    }

    private int getSelectedSegment(Point point) {
        if (selectedRegion == null) {
            return -1;
        }
        Polygon2D p = selectedRegion.polygon;
        
        if (p.npoints <= 1) {
            return -1;
        }
        
        double lastx = p.xpoints[p.npoints - 1];
        double lasty = p.ypoints[p.npoints - 1];
        double curx, cury;

        for (int i = 0; i < p.npoints; lastx = curx, lasty = cury, i++) {
            curx = p.xpoints[i];
            cury = p.ypoints[i];

            Line2D.Double line = new Line2D.Double(
                    worldToScreenX(curx),
                    worldToScreenY(cury),
                    worldToScreenX(lastx),
                    worldToScreenY(lasty));

            if (line.ptLineDist(point) < 4) {
                Rectangle r = line.getBounds();
                r.x -= 2;
                r.y -= 2;
                r.width += 4;
                r.height += 4;
                if (r.contains(point)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public void updateDisplay() {
        clear();
        drawMap();
        repaint();
    }

    public void updateCursor(MouseEvent evt) {
        if (selectVertex(evt.getPoint())) {
            setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        } else if (getSelectedSegment(evt.getPoint()) != -1) {
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        } else {
            setCursor(Cursor.getDefaultCursor());
        }
        setToolTipText(screenToWorldX(evt.getX()) + ", " + screenToWorldY(evt.getY()));
    }

    private boolean selectRegion(Point pnt) {
        selectedVertex = -1;
        for (Region r : system.map.regions) {
            if (!r.type && r.polygon.contains(screenToWorldX(pnt.x), screenToWorldY(pnt.y))) {
                selectedRegion = r;
                return true;
            }
        }

        for (Region r : system.map.regions) {
            if (r.type && r.polygon.contains(screenToWorldX(pnt.x), screenToWorldY(pnt.y))) {
                selectedRegion = r;
                return true;
            }
        }

        selectedRegion = null;
        return false;
    }

    private boolean selectVertex(Point pnt) {
        Point2D pWorld = new Point2D.Double(screenToWorldX(pnt.x), screenToWorldY(pnt.y));

        for (Region r : system.map.regions) {
            Polygon2D p = r.polygon;
            for (int i = 0; i < p.npoints; i++) {
                double x = p.xpoints[i];
                double y = p.ypoints[i];
                if (pnt.distance(worldToScreenX(x), worldToScreenY(y)) < 4) {
                    selectedRegion = r;
                    selectedVertex = i;
                    return true;
                }
            }
        }

        return false;
    }

    public void drawMap() {
        AffineTransform transform = new AffineTransform();
        transform.scale(viewScale, viewScale);
        transform.translate(-viewLon, -viewLat);

        for (LakeMap.Region region : system.map.regions) {
            Shape s = transform.createTransformedShape(region.polygon);
            getPen().setColor(Color.BLUE);
            if (region.type) {
                getPen().fill(s);
            }
        }

        for (LakeMap.Region region : system.map.regions) {
            Shape s = transform.createTransformedShape(region.polygon);
            getPen().setColor(Color.GREEN);
            if (!region.type) {
                getPen().fill(s);
            }
        }

        getPen().setColor(Color.RED);
        for (LakeMap.Region region : system.map.regions) {
            Polygon2D p = region.polygon;
            for (int i = 0; i < p.npoints; i++) {
                int x = worldToScreenX(p.xpoints[i]);
                int y = worldToScreenY(p.ypoints[i]);
                getPen().fillRect(x - 2, y - 2, 4, 4);
            }
        }

        if (selectedRegion != null) {
            Shape s = transform.createTransformedShape(selectedRegion.polygon);
            getPen().draw(s);
        }
    }

    public void addSystem(ScumbotSystem system) {
        this.system = system;
        updateDisplay();
    }

    public void setTool(Tool tool) {
        currentTool = tool;
    }

    public enum Tool {
        DRAW_LAND, DRAW_WATER, EDIT_VERTEX
    }

    public void setMapBounds(float minLat, float minLon, float maxLat, float maxLon) {

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
