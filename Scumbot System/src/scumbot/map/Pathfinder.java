/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scumbot.map;

import java.util.LinkedList;
import java.util.PriorityQueue;
import scumbot.map.MapTile;
import scumbot.system.Location;

/**
 *
 * @author MillerV
 */
public class Pathfinder {
    public static LinkedList<MapTile> findPath(LakeMap map, Location start, Location end) {
        MapTile startTile = map.getTile(start);
        MapTile endTile = map.getTile(end);
        
        if (map.getTileType(start) == MapTile.Type.LAND || map.getTileType(end) == MapTile.Type.LAND) {
            throw new RuntimeException("Either the start or end was land.");
        }

        PriorityQueue<MapTile> frontier = new PriorityQueue<MapTile>(11, new MapTile.TileComparator());
        startTile.shortestPath = new LinkedList<MapTile>();
        frontier.add(startTile);
        
        boolean success = false;

        while (!frontier.isEmpty()) {
            if (processOneEntry(frontier, map, endTile)) {
                success = true;
                break;
            }
        }
        
        if (!success) {
            throw new RuntimeException("No path to destination.");
        }
        
        return endTile.shortestPath;
    }

    private static boolean processOneEntry(PriorityQueue<MapTile> frontier, LakeMap map, MapTile dest) {
        MapTile best = frontier.remove();
        if (best == dest) {
            return true;
        }
        for (MapTile tile : best.getNeighbors()) {
            if (tile.getType() == MapTile.Type.WATER && (tile.shortestPath == null || tile.shortestPath.size() > best.shortestPath.size() + 1)) {
                tile.shortestPath = new LinkedList<MapTile>();
                tile.shortestPath.addAll(best.shortestPath);
                tile.shortestPath.add(best);
                
                if (frontier.contains(tile)) {
                    frontier.remove(tile);
                }
                frontier.add(tile);
            }
        }
        return false;
    }
}
