/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scumbot.map;

import ccre.log.Logger;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import scumbot.system.Location;

/**
 *
 * @author MillerV
 */
public class LakeMap {

    private Region currentRegion;
    public final LinkedList<Region> regions;

    private double maxx = Double.NEGATIVE_INFINITY;
    private double maxy = Double.NEGATIVE_INFINITY;

    private double minx = Double.POSITIVE_INFINITY;
    private double miny = Double.POSITIVE_INFINITY;

    private final double tileSize = 0.01; // this is the number of world units (probably lat/lon degrees) in one grid square

    private MapTile[][] tiles;

    public LakeMap() {
        regions = new LinkedList<Region>();
    }

    public Location worldToGrid(Point2D p) {
        return new Location((int) ((p.getY() - miny) / tileSize), (int) ((p.getX() - minx) / tileSize));
    }

    public Point2D.Double gridToWorld(Location l) {
        return new Point2D.Double((l.col * tileSize) + minx, (l.row * tileSize) + miny);
    }

    public boolean setTileType(Location loc, MapTile.Type type) {
        if (loc.row < tiles.length && loc.col < tiles[0].length) {
            tiles[loc.row][loc.col] = new MapTile(type, this, loc);
            return true;
        } else {
            return false;
        }
    }

    public MapTile.Type getTileType(Location loc) {
        if (loc.row < tiles.length && loc.col < tiles[0].length) {
            return tiles[loc.row][loc.col].getType();
        } else {
            return MapTile.Type.LAND;
        }
    }

    

    public void trySetWater(Location loc) {
        if (loc.row < getNumRows() && loc.col < getNumCols() && tiles[loc.row][loc.col] == null) {
            setTileType(loc, MapTile.Type.WATER);
        }
    }

    public int getNumRows() {
        return tiles.length;
    }

    public int getNumCols() {
        return tiles[0].length;
    }

    public MapTile getTile(Location loc) {
        return tiles[loc.row][loc.col];
    }

    public void createGrid() {
        for (Region region : regions) {
            Rectangle2D rect = region.polygon.getBounds2D();
            minx = Math.min(rect.getMinX(), minx);
            miny = Math.min(rect.getMinY(), miny);
            maxx = Math.max(rect.getMaxX(), maxx);
            maxy = Math.max(rect.getMaxY(), maxy);
        }
        tiles = new MapTile[(int) ((maxy - miny) / tileSize) + 1][(int) ((maxx - minx) / tileSize) + 1];

        for (Region region : regions) {
            Rectangle2D rect = region.polygon.getBounds2D();

            Location start = worldToGrid(new Point2D.Double(rect.getMinX(), rect.getMinY()));
            Location end = worldToGrid(new Point2D.Double(rect.getMaxX(), rect.getMaxY()));

            for (int r = start.row; r < end.row; r++) {
                for (int c = start.col; c < end.col; c++) {
                    if (region.polygon.contains(gridToWorld(new Location(r, c)))) {
                        if (region.type) {
                            trySetWater(new Location(r, c));
                            trySetWater(new Location(r, c - 1));
                            trySetWater(new Location(r - 1, c));
                            trySetWater(new Location(r - 1, c - 1));
                        } else {
                            setTileType(new Location(r, c), MapTile.Type.LAND);
                            setTileType(new Location(r, c - 1), MapTile.Type.LAND);
                            setTileType(new Location(r - 1, c), MapTile.Type.LAND);
                            setTileType(new Location(r - 1, c - 1), MapTile.Type.LAND);
                        }
                    }
                }
            }
        }

        for (int r = 0; r < getNumRows(); r++) {
            for (int c = 0; c < getNumCols(); c++) {
                if (tiles[r][c] == null) {
                    setTileType(new Location(r, c), MapTile.Type.LAND);
                }
            }
        }
    }

    /**
     * Write this map to a DataOutputStream. Data is written in the order of:
     *
     * number of regions (int) for each region: type (boolean) number of points
     * (int) for each point: x (double) y (double)
     *
     * @param dos the DataOutputStream to write to
     */
    public void write(DataOutputStream dos) {
        try {
            dos.writeInt(regions.size());
            //Logger.info("nregions: " + regions.size());

            for (Region region : regions) {
                dos.writeBoolean(region.type);
                //Logger.info("\ttype: " + region.type);
                dos.writeInt(region.polygon.npoints);
                //Logger.info("\tnpoints: " + region.polygon.npoints);
                for (int i = 0; i < region.polygon.npoints; i++) {
                    //Logger.info("\t\tx: " + region.polygon.xpoints[i]);
                    dos.writeDouble(region.polygon.xpoints[i]);
                    //Logger.info("\t\ty: " + region.polygon.ypoints[i]);
                    dos.writeDouble(region.polygon.ypoints[i]);
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static LakeMap read(DataInputStream dis) {

        try {
            LakeMap map = new LakeMap();
            int numRegions = dis.readInt();
            //Logger.info("nregions: " + numRegions);
            for (int i = 0; i < numRegions; i++) {
                boolean type = dis.readBoolean();
                Region r = new Region(type);
                //Logger.info("\ttype: " + type);
                int numPoints = dis.readInt();
                //Logger.info("\tnpoints: " + numPoints);
                for (int j = 0; j < numPoints; j++) {
                    double x = dis.readDouble();
                    //Logger.info("\t\tx: " + x);
                    double y = dis.readDouble();
                    //Logger.info("\t\ty: " + y);
                    r.polygon.addPoint(x, y);
                }
                map.regions.add(r);
            }
            //map.createGrid();
            return map;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static class Region {

        public Region(boolean type) {
            this.type = type;
            this.polygon = new Polygon2D();
        }

        public boolean type;
        public Polygon2D polygon;
    }
}
