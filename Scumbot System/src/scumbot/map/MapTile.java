/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scumbot.map;

import java.util.Comparator;
import java.util.LinkedList;
import scumbot.map.LakeMap;
import scumbot.system.Location;

/**
 *
 * @author vincent
 */
public class MapTile {

    private Type type;
    private LakeMap map;
    private Location location;
    public LinkedList<MapTile> shortestPath;
    
    public Location getLocation() {
        return location;
    }
    
    public static class TileComparator implements Comparator<MapTile> {
        @Override
        public int compare(MapTile t, MapTile t1) {
            return (int)Math.signum(t.shortestPath.size() - t1.shortestPath.size());
        }
    }

    public Type getType() {
        return type;
    }

    public MapTile(Type type, LakeMap map, Location location) {
        this.type = type;
        this.map = map;
        this.location = location;
    }

    public enum Type {
        LAND, WATER
    }

    public LinkedList<MapTile> getNeighbors() {
        LinkedList<MapTile> neighbors = new LinkedList<MapTile>();
        if (location.row > 0) {
            neighbors.add(map.getTile(new Location(location.row - 1, location.col)));
        }
        if (location.row < map.getNumRows()) {
            neighbors.add(map.getTile(new Location(location.row + 1, location.col)));
        }
        if (location.col > 0) {
            neighbors.add(map.getTile(new Location(location.row, location.col - 1)));
        }
        if (location.col < map.getNumCols() - 1) {
            neighbors.add(map.getTile(new Location(location.row, location.col + 1)));
        }
        return neighbors;
    }
}
