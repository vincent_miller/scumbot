/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scumbot.map;

import ccre.log.LogLevel;
import ccre.log.Logger;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;

/**
 *
 * @author MillerV
 */
public class Polygon2D implements Shape {

    public double[] xpoints;
    public double[] ypoints;

    public int npoints;

    private Rectangle2D.Double bounds;

    private static final int MIN_LENGTH = 4;

    public Polygon2D() {
        xpoints = new double[MIN_LENGTH];
        ypoints = new double[MIN_LENGTH];
    }

    public Polygon2D(double[] xpoints, double[] ypoints, int npoints) {
        if (npoints > xpoints.length || npoints > ypoints.length) {
            throw new IndexOutOfBoundsException("npoints > npoints.length || npoints > ypoints.length");
        }

        if (npoints < 0) {
            throw new NegativeArraySizeException("npoints < 0");
        }

        this.npoints = npoints;
        this.xpoints = Arrays.copyOf(xpoints, npoints);
        this.ypoints = Arrays.copyOf(ypoints, npoints);
    }

    public void reset() {
        npoints = 0;
        bounds = null;
    }

    public void invalidate() {
        bounds = null;
    }

    private void calculateBounds() {
        double minX = Double.POSITIVE_INFINITY;
        double minY = Double.POSITIVE_INFINITY;
        double maxX = Double.NEGATIVE_INFINITY;
        double maxY = Double.NEGATIVE_INFINITY;

        for (int i = 0; i < npoints; i++) {
            double x = xpoints[i];
            minX = Math.min(minX, x);
            maxX = Math.max(maxX, x);
            double y = ypoints[i];
            minY = Math.min(minY, y);
            maxY = Math.max(maxY, y);
        }

        bounds = new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
    }

    private void updateBounds(double x, double y) {
        if (x < bounds.x) {
            bounds.x = x;
        } else if (x > bounds.x + bounds.width) {
            bounds.width = x - bounds.x;
        }

        if (y < bounds.y) {
            bounds.y = y;
        } else if (y > bounds.y + bounds.height) {
            bounds.height = y - bounds.y;
        }
    }

    public void addPoint(double x, double y) {
        addPoint(x, y, npoints);
    }

    public void addPoint(Point2D p) {
        addPoint(p.getX(), p.getY());
    }
    
    public void addPoint(Point2D p, int index) {
        addPoint(p.getX(), p.getY(), index);
    }
    
    public void addPoint(double x, double y, int index) {
        if (npoints >= xpoints.length || npoints >= ypoints.length) {
            int newLength = npoints * 2;
            if (newLength < MIN_LENGTH) {
                newLength = MIN_LENGTH;
            } else if ((newLength & (newLength - 1)) != 0) {
                newLength = Integer.highestOneBit(newLength);
            }

            xpoints = Arrays.copyOf(xpoints, newLength);
            ypoints = Arrays.copyOf(ypoints, newLength);
        }
        
        for (int i = npoints - 1; i >= index; i--) {
            xpoints[i + 1] = xpoints[i];
            ypoints[i + 1] = ypoints[i];
        }

        xpoints[index] = x;
        ypoints[index] = y;
        
        npoints++;
        
        if (bounds != null) {
            updateBounds(x, y);
        }
    }

    public void removePoint(double x, double y) {
        for (int i = 0; i < npoints; i++) {
            double px = xpoints[i];
            double py = xpoints[i];
            if (x == px && y == py) {
                removePoint(i);
                return;
            }
        }
    }

    public void removePoint(Point2D p) {
        removePoint(p.getX(), p.getY());
    }

    public void removePoint(int index) {
        for (int j = index; j < npoints - 1; j++) {
            xpoints[j] = xpoints[j + 1];
            ypoints[j] = ypoints[j + 1];
        }
        npoints--;
        bounds = null;
    }

    @Override
    public Rectangle getBounds() {
        return getBounds2D().getBounds();
    }

    @Override
    public Rectangle2D getBounds2D() {
        if (npoints == 0) {
            return new Rectangle2D.Double();
        }

        if (bounds == null) {
            calculateBounds();
        }

        return bounds.getBounds2D();
    }

    @Override
    public boolean contains(double x, double y) {
        if (npoints <= 2 || !getBounds2D().contains(x, y)) {
            return false;
        }
        int hits = 0;

        double lastx = xpoints[npoints - 1];
        double lasty = ypoints[npoints - 1];
        double curx, cury;

        for (int i = 0; i < npoints; lastx = curx, lasty = cury, i++) {
            curx = xpoints[i];
            cury = ypoints[i];

            if (cury == lasty) {
                continue;
            }

            double leftx;
            if (curx < lastx) {
                if (x >= lastx) {
                    continue;
                }
                leftx = curx;
            } else {
                if (x >= curx) {
                    continue;
                }
                leftx = lastx;
            }

            double test1, test2;
            if (cury < lasty) {
                if (y < cury || y >= lasty) {
                    continue;
                }
                if (x < leftx) {
                    hits++;
                    continue;
                }
                test1 = x - curx;
                test2 = y - cury;
            } else {
                if (y < lasty || y >= cury) {
                    continue;
                }
                if (x < leftx) {
                    hits++;
                    continue;
                }
                test1 = x - lastx;
                test2 = y - lasty;
            }

            if (test1 < (test2 / (lasty - cury) * (lastx - curx))) {
                hits++;
            }
        }

        return ((hits & 1) != 0);
    }

    @Override
    public boolean contains(Point2D p) {
        return contains(p.getX(), p.getY());
    }

    @Override
    public boolean intersects(double d, double d1, double d2, double d3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean intersects(Rectangle2D r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean contains(double d, double d1, double d2, double d3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean contains(Rectangle2D rd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at) {
        return new Polygon2DPathIterator(at);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at, double d) {
        return getPathIterator(at);
    }

    private class Polygon2DPathIterator implements PathIterator {

        private final AffineTransform transform;

        private int index;

        private Polygon2DPathIterator(AffineTransform transform) {
            this.transform = transform;

            if (npoints == 0) {
                index = 1;
            }
        }

        @Override
        public int getWindingRule() {
            return WIND_EVEN_ODD;
        }

        @Override
        public boolean isDone() {
            return index > npoints;
        }

        @Override
        public void next() {
            index++;
        }

        @Override
        public int currentSegment(float[] floats) {
            if (index < npoints) {
                floats[0] = (float) xpoints[index];
                floats[1] = (float) ypoints[index];

                if (transform != null) {
                    transform.transform(floats, 0, floats, 0, 1);
                }

                return (index == 0 ? SEG_MOVETO : SEG_LINETO);
            } else {
                return SEG_CLOSE;
            }
        }

        @Override
        public int currentSegment(double[] doubles) {
            if (index < npoints) {
                doubles[0] = xpoints[index];
                doubles[1] = ypoints[index];

                if (transform != null) {
                    transform.transform(doubles, 0, doubles, 0, 1);
                }

                return (index == 0 ? SEG_MOVETO : SEG_LINETO);
            } else {
                return SEG_CLOSE;
            }
        }
    }
}
