/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package scumbot.system;

/**
 *
 * @author MillerV
 */
public class Location {
    public final int row;
    public final int col;
    
    public Location(int row, int col) {
        this.row = row;
        this.col = col;
    }
    
    public boolean equals(Location other) {
        return other.row == this.row && other.col == this.col;
    }
    
    public static float getDirection(Location l1, Location l2) {
        float x = l2.col - l1.col;
        float y = l2.row - l1.row;
        
        return (float) Math.toDegrees(Math.atan2(y, x));
    }
    
    public static float getDistance(Location l1, Location l2) {
        float x = l2.col - l1.col;
        float y = l2.row - l1.row;
        
        return (float) Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }
}
