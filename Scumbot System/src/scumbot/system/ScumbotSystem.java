package scumbot.system;

import java.util.Collection;
import scumbot.map.LakeMap;
import java.util.HashMap;
import java.util.Set;

/**
 * Objects of this class store all the data in a single deployment of Scumbot:
 * all active robots, the name of the system, and a map of the lake.
 * 
 * @author millerv
 */
public class ScumbotSystem {

    private final HashMap<String, Scumbot> bots;
    private final String name;
    public LakeMap map;
    
    public ScumbotSystem(String name, LakeMap map, HashMap<String, Scumbot> bots) {
        this.bots=bots;
        this.name=name;
        this.map=map;
    }
    
    public String getName() {
        return name;
    }
    
    public Set<String> getBotNames() {
        return bots.keySet();
    }
    
    public Scumbot getBot(String name) {
        return bots.get(name);
    }
    
    public Collection<Scumbot> getBots() {
        return bots.values();
    }
    
    public void addBot(Scumbot bot) {
        bots.put(bot.getName(), bot);
    }
    
    public void removeBot(Scumbot bot) {
        bots.remove(bot.getName());
    }
    
    public void clearBots() {
        bots.clear();
    }
}
