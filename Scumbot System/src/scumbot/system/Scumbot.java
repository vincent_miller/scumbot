/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package scumbot.system;


/**
 *
 * @author vincent
 */
public class Scumbot {
    public enum Mode {
        TELEOPERATED, AUTONOMOUS, DISABLED
    }
    
    private final String name;
    private final int[] address;
    private Mode mode;
    
    private int col;
    private int row;
    
    private int colDest;
    private int rowDest;
    
    public Scumbot(String name, int[] address) {
        this.name = name;
        this.address = address;
    }
    
    public String getName() {
        return name;
    }
    
    public int[] getAddress() {
        return address;
    }
    
    @Override
    public String toString() {
        String s = "Bot: " + name + "@";
        for (int a : address) {
            s += Integer.toHexString(a) + " ";
        }
        return s;
    }
}
