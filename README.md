Uses Common Chicken Runtime Engine. To download, do the following:

1. Download a git client (git for windows, already installed on mac)

2. Run git bash (windows) or open a terminal and run the git command (mac)

3. Run the following:

```
#!git

git clone https://bitbucket.org/vincent_miller/scumbot
cd scumbot
git submodule update --init
```

and build the projects in the following order, using NetBeans:

1. CommonChickenRuntimeEngine

2. CCRE_Obsidian

3. Scumbot System

4. Scumbot Core

5. Scumbot Monitor